﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Infotech_Zadanie_Dorusak_M.Database.Tests
{
    [TestClass()]
    public class ProductsTests
    {
        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\mdoru\Source\Repos\Infotech_Zadanie_Dorusak_M\Infotech_Zadanie_Dorusak_M\Database\db_products.mdf;Integrated Security=True";
        
        [TestMethod()]
        public void PassWhenQueryToDatabaseNotReturnNull()
        {
            SqlConnection connection = Connection.Get(connectionString);
            Products products = new(connection);
            DataTable result = products.LoadProducts();
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void PassWhenProductQueryReturnsFourColumns()
        {
            SqlConnection connection = Connection.Get(connectionString);
            Products products = new(connection);
            DataTable dataTable = products.LoadProducts();
            int result = dataTable.Columns.Count;
            Assert.AreEqual(4, result);
        }

        [TestMethod()]
        public void PassWhenProductQueryReturnsFourRows()
        {
            SqlConnection connection = Connection.Get(connectionString);
            Products products = new(connection);
            DataTable dataTable = products.LoadProducts();
            int result = dataTable.Rows.Count;
            Assert.AreEqual(4, result);
        }

        [TestMethod()]
        public void PassWhenConnectionIsSuccessfullyClosed()
        {
            SqlConnection connection = Connection.Get(connectionString);
            Products products = new(connection);
            DataTable dataTable = products.LoadProducts();
            bool result = connection.State == ConnectionState.Closed;
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void PassWhenDetailedInfoLoadedSuccessfully()
        {
            SqlConnection connection = Connection.Get(connectionString);
            Products products = new(connection);
            DataTable dataTable = products.LoadDescription(1);
            Array rowItems = dataTable.Rows[0].ItemArray;
            string result = rowItems.GetValue(0).ToString();
            Assert.AreEqual(result, "1");
        }
    }
}