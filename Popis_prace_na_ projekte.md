Najprv som krátko zaváhal či sa mám vôbec pustiť do tohto projektu pretože s C# som doteraz nemal žiadne skúsenosti.

Prvý väčší problém nastal keď som musel nainštalovať Windows. Doteraz som totiž pri bežnej činnosti na pc aj počas vývoja
fungoval na linuxe. Na vývoj som používal IntelliJ idea. Nainštaloval som teda Windows do VirtualBoxu. 
Následne som nainštaloval Visual studio code. Zistil som, že to je len textový editor. Potom som konečne nainštaloval Visual studio.
Išiel som na stránku WinForms framworku. Tam som hľadal inštalačný súbor toho frameworku. Našiel som len nejaké demá.
Až po chvíli som zistil, že WinForms je už integrovaný do Visual studia.

Začal som sa teda postupne orientovať vo Visual studio. V prvých krokoch som sa často pýtal chatGTP ako sa ktorá vec robí.
Postupne som vytvoril používateľské rozhranie aplikácie. Vo Visual studio sa mi pracuje podstatne lepšie ako v IntelliJ idea.

Na vytvorenie databázy som použil SQl server management studio. Nemal som problém ani so štruktúrou databázy. 
Som zvyknutý pracovať s databázou na Androide cez Room.

Najväčší problém sa javilo integrovať databázu do projektu. Potreboval som aby bol celý projekt prenositeľný a jednoducho spustuteľný.
Jednoduché zadanie relatívnej cesty k databáze nefunguje. Na stackoverflow som vyhľadal asi najpriamejšie riešenie:
V súbore App.config je zapísaná cesta k databáze. Spustiteľný súbor si potom cez DataDirectory túto cestu nájde.
Toto riešenie sa mi nepáči ale zatiaľ som na lepšie neprišiel.

Nepodarilo sa mi doteraz pochopiť implementovanie rozhrania IDisposable. V projekte preto zatváram spojenie s databázou database.Close.
Takéto zatváranie nepokrýva situácie keď sa spojenie nečakane preruší predtým, než sa zavolá metéda .Close.

Celkovo je pre mňa vývoj v C# o objavovaní pretože som zvyknutý na javu. Rozdiely sú však menšieho rázu a myslím, že sa dajú rýchlo naučiť.