Zadanie projektu: Pros�me V�s, aby ste vytvorili konzolov� (desktopov�/WinForm-ov�) aplik�ciu s nasleduj�cimi �pecifik�ciami:

    Hlavn� obrazovka: Aplik�cia by mala ma� hlavn� obrazovku s uv�tacou spr�vou, napr�klad "Vitajte v aplik�cii".

    Horn� li�ta s menu: V hornej �asti obrazovky by mala by� li�ta s menu, vr�tane tla�idla "Katal�g".

    Modul Katal�g: Po kliknut� na tla�idlo "Katal�g" by sa mal otvori� modul, ktor� obsahuje dva gridy oddelen� splitterom:
        Horn� Grid: Zoznam tovarov, ktor� spolo�nos� pred�va s r�znymi st�pcami (napr�klad n�zov tovaru, cena, at�.). M��ete sa in�pirova� akouko�vek obchodnou str�nkou.
        Spodn� Grid: Detail produktu, ktor� sa na��ta pri v�bere tovaru z horn�ho gridu. Navrhnite si, ak� inform�cie by mali by� zobrazen�, aby bolo rozhranie pou��vate�sky pr�vetiv� a jednoduch� na pou��vanie.

    Technick� po�iadavky:
        Pou�ite framework DevExpress (Devexpress WinForms Controls).
        D�ta by mali by� na��tan� z MS SQL datab�zy. �trukt�ru d�t si navrhnite sami.

    Dokument�cia a v�stup:
        Projekt nahrajte na GitLab, vr�tane anglickej dokument�cie alebo popisu projektu.
        Pridajte kr�tky dokument, ktor� opisuje V� postup pr�ce, ak� �asti boli n�ro�n�, s ��m ste sa museli popasova� at�.
        Vytvorte diagram datab�zovej �trukt�ry.

Tento projekt n�m pom��e lep�ie pochopi� Va�e schopnosti a z�rove� V�m poskytne pr�le�itos� uk�za� Va�u kreativitu a technick� zru�nosti. V malej miere V�m uk�e aj to, s ��m sa budete v pr�ci zaobera�. Deadline je do 17.12.2023. Te��me sa na V� projekt a �akujeme V�m za V� z�ujem pracova� s nami.