﻿using Infotech_Zadanie_Dorusak_M.Database;
using System.Data;

namespace Infotech_Zadanie_Dorusak_M.UI
{
    /// <summary>
    /// Class DataGridViewDetails represents methods to operations with GridView.
    /// </summary>
    /// <param name="database">Connection with inromations about products.</param>
    /// <param name="dataGridViewDetails">DataGridViewDetails object where information should be displayed.</param>
    public class DataGridViewDetails(Products database, DataGridView dataGridViewDetails)
    {
        private readonly Products _database = database;
        private readonly DataGridView _dataGridViewDetails = dataGridViewDetails;
        /// <summary>
        /// Shows detailed informations for selected product from products based on its ID.
        /// </summary>
        /// <param name="id">ID of product for which detailed informations should be loaded.</param>
        public void ShowDetails(int id)
        {
            DataTable dataTable = _database.LoadDescription(id);
            _dataGridViewDetails.DataSource = dataTable;
            DataGridViewColumn columnIdDetails = _dataGridViewDetails.Columns[0];
            columnIdDetails.Visible = false;
        }
    }
}
