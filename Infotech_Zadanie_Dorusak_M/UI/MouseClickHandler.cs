﻿using Infotech_Zadanie_Dorusak_M.Database;
using Microsoft.Data.SqlClient;

namespace Infotech_Zadanie_Dorusak_M.UI
{
    /// <summary>
    /// CLass represents methods for operations with handling of clicking mouse.
    /// </summary>
    /// <param name="dataGridProducts">DataGridView object from which id of product will be taken.</param>
    /// <param name="dataGridDetails">DataGridView object details will be filled into.</param>
    internal class MouseClickHandler(DataGridView dataGridProducts, DataGridView dataGridDetails)
    {
        private readonly DataGridView _dataGridProducts = dataGridProducts;
        private readonly DataGridView _dataGridDetails = dataGridDetails;

        /// <summary>
        /// From given DataGridViewCellEventArgs takes value of id cell. Establishes _connection to _database.
        /// Retrieve details for product with specified id. Details are then showed in GridView.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="connection"></param>
        internal void LoadDetails(DataGridViewCellEventArgs e, SqlConnection connection)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex < 0)
            {
                return;
            }
            DataGridViewRow row = _dataGridProducts.Rows[rowIndex];
            DataGridViewCell firstCell = row.Cells[0];
            string idProduct = row.Cells[0].Value.ToString();
            Products products = new(connection);
            DataGridViewDetails details = new(products, _dataGridDetails);
            int id;
            try
            {
                id = int.Parse(idProduct);
            }
            catch (Exception)
            {
                return;
            }
            details.ShowDetails(id);
        }
    }

}
