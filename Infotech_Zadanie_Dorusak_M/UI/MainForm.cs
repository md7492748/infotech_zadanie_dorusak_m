﻿using System.Configuration;
using System.Data;
using Infotech_Zadanie_Dorusak_M.Database;
using Infotech_Zadanie_Dorusak_M.UI;
using Microsoft.Data.SqlClient;

namespace Infotech_Zadanie_Dorusak_M
{
    /// <summary>
    /// Class represents methods to work with user interface components.
    /// </summary>
    public partial class MainForm : Form
    {
        private SqlConnection _connection;
        /// <summary>
        /// Main constructor for creating components.
        /// </summary>
        internal MainForm()
        {
            InitializeComponent();
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(welcomeMessage);
            ConectDatabase();
        }

        /// <summary>
        /// Find path to local database file from attached file App.config. Then establishes connection to that file.
        /// </summary>
        private void ConectDatabase()
        {
            var appSetting = ConfigurationManager.AppSettings["dataDir"];
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var path = Path.Combine(baseDir, appSetting);
            var fullPath = Path.GetFullPath(path);
            AppDomain.CurrentDomain.SetData("DataDirectory", fullPath);
            string connectionString = ConfigurationManager.ConnectionStrings["EmbeddedDatabaseConnectionString"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Shows components after click to Catalog menu item. Load products from database and fill the to GridView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CatalogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(splitContainer1);
            Products products = new(_connection);
            DataTable dataTable = products.LoadProducts();
            gridViewProducts.DataSource = dataTable;
            columnIdProducts = gridViewProducts.Columns[0];
            columnIdProducts.Visible = false;
        }

        /// <summary>
        /// Shows welcome message after click to intro menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IntroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            mainPanel.Controls.Add(welcomeMessage);
        }

        /// <summary>
        /// After mouse click to product in table loads detailed description to second table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridViewProducts_MouseClick(object sender, DataGridViewCellEventArgs e)
        {
            MouseClickHandler handler = new(gridViewProducts, gridViewDetails);
            handler.LoadDetails(e, _connection);
        }
    }
}
