﻿namespace Infotech_Zadanie_Dorusak_M
{
    partial class MainForm
    {
        ///  Required designer variable.
        private System.ComponentModel.IContainer components = null;

        ///  Clean up any resources being used.
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            menuStrip = new MenuStrip();
            introToolStripMenuItem = new ToolStripMenuItem();
            catalogToolStripMenuItem = new ToolStripMenuItem();
            mainPanel = new Panel();
            splitContainer1 = new SplitContainer();
            gridViewProducts = new DataGridView();
            gridViewDetails = new DataGridView();
            welcomeMessage = new Label();
            form1BindingSource1 = new BindingSource(components);
            form1BindingSource = new BindingSource(components);
            bindingSource1 = new BindingSource(components);
            bindingSource2 = new BindingSource(components);
            menuStrip.SuspendLayout();
            mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)gridViewProducts).BeginInit();
            ((System.ComponentModel.ISupportInitialize)gridViewDetails).BeginInit();
            ((System.ComponentModel.ISupportInitialize)form1BindingSource1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)form1BindingSource).BeginInit();
            ((System.ComponentModel.ISupportInitialize)bindingSource1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)bindingSource2).BeginInit();
            SuspendLayout();
            // 
            // menuStrip
            // 
            menuStrip.Items.AddRange(new ToolStripItem[] { introToolStripMenuItem, catalogToolStripMenuItem });
            menuStrip.Location = new Point(0, 0);
            menuStrip.Name = "menuStrip";
            menuStrip.Size = new Size(1174, 24);
            menuStrip.TabIndex = 0;
            menuStrip.Text = "menuStrip1";
            // 
            // introToolStripMenuItem
            // 
            introToolStripMenuItem.Name = "introToolStripMenuItem";
            introToolStripMenuItem.Size = new Size(47, 20);
            introToolStripMenuItem.Text = "Úvod";
            introToolStripMenuItem.Click += IntroToolStripMenuItem_Click;
            // 
            // catalogToolStripMenuItem
            // 
            catalogToolStripMenuItem.Name = "catalogToolStripMenuItem";
            catalogToolStripMenuItem.Size = new Size(59, 20);
            catalogToolStripMenuItem.Text = "Katalóg";
            catalogToolStripMenuItem.Click += CatalogToolStripMenuItem_Click;
            // 
            // mainPanel
            // 
            mainPanel.Controls.Add(splitContainer1);
            mainPanel.Controls.Add(welcomeMessage);
            mainPanel.Location = new Point(0, 27);
            mainPanel.Name = "mainPanel";
            mainPanel.Size = new Size(1174, 625);
            mainPanel.TabIndex = 1;
            // 
            // splitContainer1
            // 
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(gridViewProducts);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(gridViewDetails);
            splitContainer1.Size = new Size(1174, 625);
            splitContainer1.SplitterDistance = 308;
            splitContainer1.TabIndex = 1;
            // 
            // gridViewProducts
            // 
            gridViewProducts.AllowUserToAddRows = false;
            gridViewProducts.AllowUserToDeleteRows = false;
            gridViewProducts.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gridViewProducts.Location = new Point(0, 0);
            gridViewProducts.Name = "gridViewProducts";
            gridViewProducts.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridViewProducts.Size = new Size(1174, 311);
            gridViewProducts.TabIndex = 0;
            gridViewProducts.CellClick += GridViewProducts_MouseClick;
            // 
            // gridViewDetails
            // 
            gridViewDetails.AllowUserToAddRows = false;
            gridViewDetails.AllowUserToDeleteRows = false;
            gridViewDetails.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gridViewDetails.Location = new Point(0, 3);
            gridViewDetails.Name = "gridViewDetails";
            gridViewDetails.ReadOnly = true;
            gridViewDetails.Size = new Size(1174, 310);
            gridViewDetails.TabIndex = 0;
            // 
            // welcomeMessage
            // 
            welcomeMessage.AutoSize = false;
            welcomeMessage.TextAlign = ContentAlignment.MiddleCenter;
            welcomeMessage.Dock = DockStyle.Fill;
            welcomeMessage.Location = new Point(551, 299);
            welcomeMessage.Name = "welcomeMessage";
            welcomeMessage.Size = new Size(95, 15);
            welcomeMessage.TabIndex = 0;
            welcomeMessage.Text = "Vitajte v aplikácii. Zobrazuje zoznam produktov a detailné informácie o produktoch." +
                "\n Pokračujte klinutím na položku Katalóg v hornom menu.";
            // 
            // form1BindingSource1
            // 
            form1BindingSource1.DataSource = typeof(MainForm);
            // 
            // form1BindingSource
            // 
            form1BindingSource.DataSource = typeof(MainForm);
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1174, 651);
            Controls.Add(mainPanel);
            Controls.Add(menuStrip);
            MainMenuStrip = menuStrip;
            Name = "MainForm";
            Text = "App by Dorusak Marcel";
            menuStrip.ResumeLayout(false);
            menuStrip.PerformLayout();
            mainPanel.ResumeLayout(false);
            mainPanel.PerformLayout();
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)gridViewProducts).EndInit();
            ((System.ComponentModel.ISupportInitialize)gridViewDetails).EndInit();
            ((System.ComponentModel.ISupportInitialize)form1BindingSource1).EndInit();
            ((System.ComponentModel.ISupportInitialize)form1BindingSource).EndInit();
            ((System.ComponentModel.ISupportInitialize)bindingSource1).EndInit();
            ((System.ComponentModel.ISupportInitialize)bindingSource2).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }
        #endregion

        private MenuStrip menuStrip;
        private ToolStripMenuItem introToolStripMenuItem;
        private ToolStripMenuItem catalogToolStripMenuItem;
        private Panel mainPanel;
        private Label welcomeMessage;
        private SplitContainer splitContainer1;
        private DataGridView gridViewProducts;
        private BindingSource bindingSource1;
        private BindingSource bindingSource2;
        private BindingSource form1BindingSource;
        private BindingSource form1BindingSource1;
        private DataGridView gridViewDetails;
        private DataGridViewColumn columnIdProducts;
        private DataGridViewColumn columnIdDetails;
    }
}

