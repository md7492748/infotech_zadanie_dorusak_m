﻿using Microsoft.Data.SqlClient;

namespace Infotech_Zadanie_Dorusak_M.Database
{
    /// <summary>
    /// Helper class to provide connection to SQL database.
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// Tries to establish connection with given connection string.
        /// </summary>
        /// <param name="connectionString">String with working parameters to connect to database.</param>
        /// <returns>object Connection</returns>
        /// <exception cref="Exception">If connection cannot be established.</exception>
        public static SqlConnection Get(string connectionString) 
        {
            SqlConnection? sqlConnection;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("SQL _connection cannot be established. Bad connection string or database does not exists." + e.Message);
            }
            return sqlConnection;
        }
    }
}
