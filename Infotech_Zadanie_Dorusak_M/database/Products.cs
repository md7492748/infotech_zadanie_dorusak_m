﻿using System.Data;
using Microsoft.Data.SqlClient;

namespace Infotech_Zadanie_Dorusak_M.Database
{
    /// <summary>
    /// Class Products represents methods to access informations about products stored in _database.
    /// </summary>
    public class Products
    {
        private readonly string _shortInfo = "SELECT * FROM dbo.table_products";
        private string _detailedInfo = "SELECT * FROM dbo.table_products_details WHERE id=";
        private readonly SqlConnection _connection;
        private DataTable _dataTable = new();

        /// <summary>
        /// Constructs object Product with _connection to products.
        /// </summary>
        /// <param name="connection">SqlConnection to products.</param>
        public Products(SqlConnection connection)
        {
            this._connection = connection;
        }

        /// <summary>
        /// Provides basic data from products to all products.
        /// </summary>
        /// <returns>Returns object DataTable with information about products.
        /// Returns empty table if connection to database is broken.</returns>
        public DataTable LoadProducts()
        {
            _connection.Open();
            SqlDataAdapter sqlDataAdapter = new(_shortInfo, _connection);
            sqlDataAdapter.Fill(_dataTable);
            _connection.Close();
            return _dataTable;
        }

        /// <summary>
        /// Provides detailed data from products to desired product.
        /// </summary>
        /// <param name="id">íd number for which product you want detailed information.</param>
        /// <returns>Returns object DataTable with desired information about product.
        /// Returns empty table if connection to database is broken.</returns>
        public DataTable LoadDescription(int id)
        {
            _connection.Open();
            _detailedInfo += id;
            SqlDataAdapter sqlDataAdapter = new(_detailedInfo, _connection);
            sqlDataAdapter.Fill(_dataTable);
            _connection.Close();
            return _dataTable;
        }
    }

}
